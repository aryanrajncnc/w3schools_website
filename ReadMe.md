# What you need
- [Intellij Idea](https://www.jetbrains.com/idea/) or [Visual Studio code](https://code.visualstudio.com/)
- [Setup Git on your system](https://git-scm.com/downloads)
- Create an account on Gitlab or Github

# Getting started
**Step 1:** Create a blank project on your favourite IDE or text editor  
**Step 2:** Setup a blank project on gitlab  
**Step 3:** Connect the two together  
**Step 4:** Let the coding begin!  

# Components from w3schools
Sidenav:  
https://www.w3schools.com/howto/howto_js_sidenav.asp

Slideshow:  
https://www.w3schools.com/howto/howto_js_slideshow.asp

Login Modal:  
https://www.w3schools.com/howto/howto_css_login_form.asp

Responsive top nav:  
https://www.w3schools.com/howto/howto_js_topnav_responsive.asp

Parallax:  
https://www.w3schools.com/howto/howto_css_parallax.asp

Animated buttons:  
https://www.w3schools.com/howto/howto_css_animate_buttons.asp

Scroll to top button:  
https://www.w3schools.com/howto/howto_js_scroll_to_top.asp

Scroll progress indicator:  
https://www.w3schools.com/howto/howto_js_scroll_indicator.asp

Website layout:  
https://www.w3schools.com/css/css_website_layout.asp